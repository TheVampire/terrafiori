function checkEmail(){
    if($("#id_email").val() == ''){
        $("#id_email").addClass('wrong-field');
        return false;
    }
    if($("#id_email").val() != '' && ($("#id_email").val().indexOf('@') == -1 || $("#id_email").val().indexOf('.') == -1)){
        $("#id_email").addClass('wrong-field');
        return false;
    }
    $("#id_email").removeClass('wrong-field');
    return true;
}

function updateCartSize(){
    $.ajax({
        type: "GET",
        url: "/cart/size/",
        success: function(r){
            if (r != '0')
            {
                $(".basket-top").text(r);
            }
            else{
                $(".basket-top").text('Пуста');
            }
        }
    })
}

function checkOrderForm(){
    var first_error = null;
    $('.order-req').each(function(i, v){
        if($(v).attr('id') == 'id_email'){
            if(checkEmail()){
                $(v).removeClass('wrong-field');
            }
            else{
                if(!first_error){
                    first_error = $(v);
                }
            }
        }
        else{
            if($(v).val() == ''){
                $(v).addClass('wrong-field');
                if(!first_error){
                    first_error = $(v);
                }
            }
            else {
                $(v).removeClass('wrong-field');
            }
        }
    });

    if(first_error){
        $('html, body').animate({
            scrollTop: $(first_error).offset().top
        }, 500);
        return false;
    }
    return true;
}

function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

$(function(){

    $(".fancybox").fancybox();

    $(".fr-nav-chzn").click(function(){
        $(".fr-nav-vars").slideToggle(500);
        return false;
    });

    $(".fr-nav-vars a").click(function(){
        $(".fr-nav-vars").slideUp(500);
        $(".fr-nav-chzn").text($(this).text());
        scrollToAnchor($(this).attr('href').split('#')[1]);
        return false;
    });

    $(document).on('submit', "form.fr-form", function(){
        $.ajax({
            type: 'POST',
            url: '/franchise/',
            data: $(this).serializeArray(),
            success: function(){
                $(".modal").modal('hide');
                $("#fr-order-id").modal('show');
            }
        });
        return false;
    });

    if(!!window.chrome){
        var t = "<video width=\"960\" height=\"400\" poster=\"/static/img/previue.jpg\" muted=\"muted\" autoplay=\"autoplay\" loop=\"loop\" tabindex=\"0\" width=\"100%\" id=\"fr-video\">"+
          "<source id=\"fr-video-webm\" src=\"/static/video/terrafiori.webm\" type='video/webm; codecs=\"vp8, vorbis\"' />"+
         "</video>";
        $("#fr-video").replaceWith(t);
    }

//    $("#countdown").flipcountdown({
//        size: 'sm',
//        showDay: false,
//        beforeDateTime: $("#timer-init-id").text()
//    });

    $("#login-modal-id form").submit(function(){
        $.ajax({
            type: "POST",
            url: "/login/ajax/",
            data: $(this).serializeArray(),
            success: function(resp){
                if (resp.errors){
                    var t = '';
                    for (var k in resp.errors){
                        t += '<div>'+k+': '+resp.errors[k]+'</div>';
                    }
                    $(".login-modal-info").html(t);
                    $("#login-modal-id form").slideUp(500);
                    $(".login-modal-info").slideDown(500);
                    setTimeout(function(){
                        $("#login-modal-id form").slideDown(500);
                        $(".login-modal-info").text('');
                        $(".login-modal-info").slideUp(500);
                    }, 5000);
                }
                else{
                    window.location.reload();
                }
            }
        });
        return false;
    });

    $("#order-call-id form").submit(function(){
        $.ajax({
            type: "POST",
            url: '/order/call/',
            data: $(this).serializeArray(),
            success: function(resp){
                if (resp.errors){
                    var t = '';
                    for (var k in resp.errors){
                        t += '<div>'+k+': '+resp.errors[k]+'</div>';
                    }
                    $(".order-call-info").html(t);
                    $("#order-call-id form").slideUp(500);
                    $(".order-call-info").slideDown(500);
                    setTimeout(function(){
                        $("#order-call-id form").slideDown(500);
                        $(".order-call-info").text('');
                        $(".order-call-info").slideUp(500);
                    }, 5000);
                }
                else{
                    $("#order-call-id form").slideUp(500);
                    $(".order-call-info").text(resp);
                    $(".order-call-info").slideDown(500);
                    setTimeout(function(){
                        $("#order-call-id form").slideDown(500);
                        $(".order-call-info").text('');
                        $(".order-call-info").slideUp(500);
                        $("#order-call-id form input[type='text']").val('');
                    }, 5000);
                }
            }
        });
        return false;
    });

    $("select.item-color").change(function(){
        var color_pk = $(this).val();
        if($("#item-image-for-color-"+color_pk).length){
            $("#item-image-for-color-"+color_pk).click();
        }
    });

    $("#reg-modal-success-link-id").click(function(){
        $("#reg-modal-id").modal('hide');
        $("#reg-success-modal-id").modal('show');
        return false;
    });

    if($(".news-thumbs").length){
        var owl = $(".news-thumbs");

        $(".news-thumbs").owlCarousel({
            items: 3,
            loop: true,
            margin: 5
        });

        $(".news-thumbs-left").click(function () {
            owl.trigger('prev.owl.carousel');
            return false;
        });
        $(".news-thumbs-right").click(function () {
            owl.trigger('next.owl.carousel');
            return false;
        });
    }

    if($(".fr-goods").length){
        var owl_goods = $(".fr-goods");

        owl_goods.owlCarousel({
            items: 4,
            loop: true
        });

        $(".fr-goods-left").click(function () {
            owl_goods.trigger('prev.owl.carousel');
            return false;
        });
        $(".fr-goods-right").click(function () {
            owl_goods.trigger('next.owl.carousel');
            return false;
        });
    }

    if($(".about-clients").length){
        var owl_clients = $(".about-clients-carousel");

        owl_clients.owlCarousel({
            items: 4,
            loop: true,
            margin: 10
        });
        $(".about-clients .fr-goods-left").click(function () {
            owl_clients.trigger('prev.owl.carousel');
            return false;
        });
        $(".about-clients .fr-goods-right").click(function () {
            owl_clients.trigger('next.owl.carousel');
            return false;
        });
    }

    if($(".fr-clients").length){
        var owl_clients = $(".fr-clients");

        owl_clients.owlCarousel({
            items: 4,
            loop: true,
            margin: 10
        });

        $(".fr-clients-wrapper .fr-goods-left").click(function () {
            owl_clients.trigger('prev.owl.carousel');
            return false;
        });
        $(".fr-clients-wrapper .fr-goods-right").click(function () {
            owl_clients.trigger('next.owl.carousel');
            return false;
        });
    }
    if($(".fr-works").length){
        var owl_works = $(".fr-works");

        owl_works.owlCarousel({
            items: 1,
            loop: true
        });

        $(".fr-works-block .fr-goods-left").click(function () {
            owl_works.trigger('prev.owl.carousel');
            return false;
        });
        $(".fr-works-block .fr-goods-right").click(function () {
            owl_works.trigger('next.owl.carousel');
            return false;
        });
    }

    $("#subscribe-form-id").submit(function(){
        $.ajax({
            type: "POST",
            url: '/subscribe/ajax/',
            data: $(this).serializeArray(),
            success: function(resp){
                $("#subscribe-form-id").slideUp();
                $(".success-subscriber").slideDown();
            }
        });
        return false;
    });

    $("#reg-form-id").submit(function(){
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serializeArray(),
            error: function(r){
                $("#reg-form-id").before(r.responseText);
            },
            success: function(){
                $("#reg-modal-id").modal('show');
                $(".reg-errors").remove();
                $("#reg-form-id input[type='text']").val('');
            }
        });
        return false;
    });

    $("#fb-form-id").submit(function(){
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serializeArray(),
            error: function(r){
                $("#fb-form-id").before(r.responseText);
            },
            success: function(){
                $("#fb-modal-id").modal('show');
                $(".fb-errors").remove();
                $("#fb-form-id").find("input[type='text'],textarea").val('');
            }
        });
        return false;
    });

    $("#login-bottom-form-id").submit(function(){
        if($(this).find('input[type="text"]').val() != '' && $(this).find('input[type="password"]').val() != ''){
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(r){
                    if(r == 'Logged'){
                        location.href = $("#login-bottom-form-id input[name='next']").val();
                    }
                    else{
                        $("#login-alert-id").modal('show');
                    }
                }
            })
        }
        return false;
    });
    $("#login-change-price-type-form-id").submit(function(){
        if($(this).find('input[type="text"]').val() != '' && $(this).find('input[type="password"]').val() != ''){
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(r){
                    if(r == 'Logged'){
                        window.location.reload();
                    }
                }
            })
        }
        return false;
    });

    $(".buy-count").styler({
        selectSearch: false
    });

    $("select.buy-type").bind('change', function(){
            var buy_type = $("select.buy-type").val();
            if(buy_type == 'rent')
            {
                $("#change-price-type-modal-id").modal('show');
                $("#change-price-type-modal-desc-rent").show();
                $("#change-price-type-modal-link-rent").show();
            }
            else{
                if(buy_type == 'opt')
                {
                    $("#change-price-type-modal-id").modal('show');
                    $("#change-price-type-modal-desc-opt").show();
                    $("#change-price-type-modal-link-opt").show();
                }
                else {
                    location.reload();
                }
            }
        }
);

    $(".buy-type").styler({
        selectSearch: false
    });
    $(".item-color").styler({
        selectSearch: false
    });

    if($(".cart-items table").length)
    {
        var d = new Date();
        d = d.getTime();
        if (jQuery('#reloadValue').val().length == 0)
        {
            jQuery('#reloadValue').val(d);
            jQuery('body').show();
        }
        else
        {
            jQuery('#reloadValue').val('');
            location.reload();
        }
    }

    $("#order-modal .btn-dark").click(function(){
        $.ajax({
            url :'/cart/clear/',
            type: "GET",
            success: function(){
                location.href = $(".cart-item-more a:last").attr('href');
            }
        });
    });

    $("#id_phone").mask('+7 (999) 999-99-99');
    $("#id_idx").mask('999999');

    $("#id_email").blur(function(){
        checkEmail();
    });

    $(".cart-item .desc-text").trunk8({
        lines: 1,
        fill: ''
    });

    function recalSummary(){
        var sm = $(".origin-summary").text();
        sm = parseFloat(sm);
        if($(".cart-city .cart-money").text()){
            sm += parseFloat($(".cart-city .cart-money").text());
        }
        if($(".cart-promo .cart-money").text()){
            sm += parseFloat($(".cart-promo .cart-money").text());
        }
        $(".cart-summary .cart-money").text(sm.toFixed(2).replace(".", ","));
    }

    $(".cart-del a").click(function(){
        $(this).parents('tr').remove();
        var more_link = $(this).parents('tr').eq(0).find('.cart-item-more a').attr('href');
        $.ajax({
            url: '/cart/item/del/' + $(this).attr('rel') + '/',
            type: "POST",
            success: function(resp){
                if($('.cart-item').length == 0)
                {
                    location.href = more_link;
                }
                $(".origin-summary").text(resp);
                recalSummary();
            }
        });
        updateCartSize();
        return false;
    });

    $(".cart-items #id_city").change(function(){
        var cpk = $(".cart-items #id_city option:selected").val();
        var city_name = $(".cart-items #id_city option:selected").text();
        $.ajax({
            'type': "POST",
            'url': '/city/check/',
            data: {
                'city_pk': cpk
            }
            ,success: function(resp){
                if(resp != '-1'){
                    if(city_name != 'Самовывоз' && $(".btn-pay").val() != 'opt'){
                        $(".delivery-money").show();
                        $(".free-delivery-block").hide();
                        $(".paid-delivery-block").show();
                        $(".cart-time").text(resp.time);
                        $(".cart-city .cart-money").text(resp.price.replace(".", ","));
                        $(".btn-pay").text("Перейти к платежной системе");
                        $(".cart-city .currency").show();
                        $("#id_pay_type").val('Robokassa');
                        $(".cart-pay-way").show();

                        $("#id_idx").show();
                        $("#id_idx").addClass('order-req');
                        $("#id_address").show();
                        $("#id_address").addClass('order-req');
                    }
                    else{
                        $(".free-delivery-block").show();
                        $(".paid-delivery-block").hide();
                        $(".delivery-money").hide();
                        $(".cart-city .cart-money").text(0);
                        $(".btn-pay").text("Оформить заказ");
                        $("#id_pay_type").val('Наличные');
                        $(".cart-pay-way").hide();

                        $("#id_idx").removeClass('order-req');
                        $("#id_idx").hide();
                        $("#id_address").removeClass('order-req');
                        $("#id_address").hide();
                    }
                    recalSummary();
                }
            }
        })
    });
    $("#id_city").change();

    $(".btn-promo-check").click(function(){
        $.ajax({
            type: "POST",
            url: '/promo/check/',
            data: {
                'code': $("#promocode").val()
            },
            success: function(resp){
                if(resp != '-1'){

                    $(".cart-promo .cart-money").text('-'+resp.sum.toFixed(2).replace(".", ","));
                    $(".cart-promo .currency").show();
                    $("#id_promo").val(resp.id);
                }
                else{
                    $(".cart-promo .cart-money").text('');
                    $(".cart-promo .currency").hide();
                }
                recalSummary();
            }
        });
        return false;
    });

    if($("#clear-cart-id").length){
        var tf = $("#clear-cart-id");
        $.ajax({
            type: "POST",
            url: tf.attr("action"),
            data: tf.serializeArray()
        })
    }

    $(".btn-pay").click(function(){
        if(!checkOrderForm()) return false;
        if($(".btn-pay").val() == 'rent' && parseInt($(".cart-summary .cart-money").text()) < 4500){
            $("#rent-min-price-warning-id").removeClass('hidden');
            setTimeout(function(){
                $("#rent-min-price-warning-id").fadeOut(500);
                $("#rent-min-price-warning-id").addClass('hidden');
            }, 1000);
        }
        else if($(".btn-pay").val() == 'opt' && parseInt($(".cart-summary .cart-money").text()) < 50000){
            $("#opt-min-price-warning-id").removeClass('hidden');
            setTimeout(function(){
                $("#opt-min-price-warning-id").fadeOut(500);
                $("#opt-min-price-warning-id").addClass('hidden');
            }, 1000);
        }
        else{
            $.ajax({
                'url': $("#cart-form-id").attr('action'),
                type: 'POST',
                data: $("#cart-form-id").serializeArray(),
                success: function(resp){
                    yaCounter25536599.reachGoal('order');
                    if($(".free-delivery-block:visible").length || $(".btn-pay").val() == 'rent'){
                        $("#order-modal .modal-body span.order-email").text($("#id_email").val());
                        $("#order-modal").modal('show');
                        $("#order-modal").on('hide.bs.modal', function(){
                            location.href = '/';
                        });
                    }
                    else{
                        yaCounter25536599.reachGoal('topay');
                        $(".cart-pay").append(resp.form);
                        $("#pay-form-id").submit();
                    }
                }
            });
        }
        return false;
    });

    $(".btn-buy").click(function(){
        $.ajax({
            url: '/cart/add/',
            type: "POST",
            data: {
                'pk': $(this).attr('data-item-pk'),
                'count': $(".buy-block select").val(),
                'item_color': $("select.item-color").val()
            },success: function(){

            }
        })
    });

//    $(".item-big-pic img").elevateZoom();

    $(".buy-send").click(function(){
        if($('#buy-form input[name="email"]').val() == '')
        {
            $(".required-info").show();
        }
        else {
            $(".required-info").hide();
            $.ajax({
                url: $("#buy-form").attr('action'),
                type: "POST",
                data: $('#buy-form').serializeArray(),
                success: function(resp){
                    if(resp == 'OK'){
                        $('#buy-form .buy-data').slideUp();
                        $(".success-info").slideDown();
                        $(".buy-send").hide();
                    }
                    if (resp == 'ERROR')
                    {
                        $('#buy-form .buy-data').slideUp();
                        $(".error-info").slideDown();
                        $(".buy-send").hide();
                    }
                    setTimeout(function(){
                        $(".success-info").slideUp();
                        $(".error-info").slideUp();
                        $("#buy-form .buy-data").slideDown();
                        $(".buy-send").show();
                    }, 5000);
                }
            })
        }
    });

    $("#price-filter-form input").click(function(){
        if(!$("#price-filter-form input:checked").length){
            $("#price-filter-form").append("<input type='hidden' name='price_filter' value='reset' />");
        }
        $("#price-filter-form").submit();
    });

    $(document).on('click', ".item-thumbs a", function(){
        $(".chosen").removeClass('chosen');
        $(this).addClass('chosen');
        var l = $(this).attr('href');
        $('.item-big-pic img').attr('src', l);
//        $('.item-big-pic img').attr('data-zoom-image', $(this).attr('rel'));

//        $(".zoomContainer").remove();
//        $(".item-big-pic img").removeData('zoom-image');
//
//        $(".item-big-pic img").elevateZoom();

        return false;
    });

    $(".news-thumbs a").click(function(){
        $(".chosen").removeClass('chosen');
        $(this).addClass('chosen');
        var l = $(this).attr('href');
        $('.news-big-pic img').attr('src', l);
        return false;
    });

    $(".feedback-send").click(function(){
        $.ajax({
            type: 'POST',
            url: '/send/',
            data: $("#feedbackForm").serializeArray(),
            success: function(){
                $("#feedbackModal").modal('hide');
            }
        })
    });

});