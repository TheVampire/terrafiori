from django.conf.urls import patterns, include, url
from app.views import FeedbackView
from django.views.decorators.csrf import csrf_exempt

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
#    url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),


    url(r'^send/$', csrf_exempt(FeedbackView.as_view()), name='send'),

#    url(r'^send/$', csrf_exempt(FeedbackView.as_view()), name='send'),

    url(r'^', include('app.urls')),
    # url(r'^terrafiori/', include('terrafiori.foo.urls')),
    url(r'^robokassa/', include('robokassa.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
#    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^redactor/', include('redactor.urls')),

)
