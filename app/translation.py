# -*- coding: utf-8 -*-

from modeltranslation.translator import translator, TranslationOptions
from app.models import Category, SpecOffer, Item, ItemImage

class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)

class SpecOfferTranlsationOptions(TranslationOptions):
    fields = ('name', 'desc')

class ItemTranlsationOptions(TranslationOptions):
    fields = ('name', 'desc')

class ItemImageTranlsationOptions(TranslationOptions):
    fields = ('image', )

translator.register(Category, CategoryTranslationOptions)
translator.register(SpecOffer, SpecOfferTranlsationOptions)
translator.register(Item, ItemTranlsationOptions)
translator.register(ItemImage, ItemImageTranlsationOptions)