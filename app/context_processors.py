from cart import Cart

def clear_cart(request):
    if request.path == '/robokassa/success/':
        cart = Cart(request)
        cart.clear()
    return {}