from app.models import *
from django.contrib import admin
#from djrill import DjrillAdminSite
#
#admin.site = DjrillAdminSite()


from import_export import resources
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline

# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin

from app.models import CustomUser, DeliveryTextBlock
from app.forms import UserChangeForm, UserCreationForm


class UserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm


    list_display = ('email', 'username', 'is_admin', 'is_rozn', 'is_rent', 'is_opt')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'username', 'password')}),
        ('Personal info', {'fields': ('date_of_birth', 'first_name', 'last_name', 'middle_name')}),
        ('Permissions', {'fields': ('is_admin','is_opt', 'is_rozn', 'is_rent')}),
        ('Important dates', {'fields': ('last_login',)}),
        ('Additional info', {'fields': ('phone','idx', 'address', 'city', )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'date_of_birth', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(CustomUser, UserAdmin)
admin.site.unregister(Group)

class OrderCityResource(resources.ModelResource):
    class Meta:
        model = OrderCity
#        fields = ['name', 'price', 'days_min', 'days_max']

class OrderCityAdmin(ImportExportModelAdmin):
    resource_class = OrderCityResource
    pass


# class ItemImageInline(admin.TabularInline):
class ItemImageInline(TranslationTabularInline):
    model = ItemImage
    extra = 3

class NewsPhotoInline(admin.TabularInline):
    model = NewsPhoto
    extra = 3

class NewsAdmin(admin.ModelAdmin):
    inlines = [NewsPhotoInline, ]

class ItemAdmin(TranslationAdmin):
    filter_horizontal = ['specs', 'categories', 'addon_items', 'colors']
    inlines = [ItemImageInline, ]
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'price')
    search_fields = ['name',]
    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }

class AutoSlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

class CategoryAdmin(AutoSlugAdmin, TranslationAdmin):
    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }

class OrderAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'get_user_status', 'payed', 'total_summary']

class PromoCodeAdmin(admin.ModelAdmin):
    list_display = ['code', 'sum']

admin.site.register(SpecOffer, CategoryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ItemColor)
admin.site.register(Item, ItemAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(DeliveryTextBlock)
admin.site.register(AboutTextBlock)
admin.site.register(TeamMember)
admin.site.register(AboutPortfolioItem)
admin.site.register(Banner)
admin.site.register(TextBlock)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderCity, OrderCityAdmin)
admin.site.register(PromoCode, PromoCodeAdmin)
admin.site.register(MailTemplate)
admin.site.register(Images4Contacts)
admin.site.register(Images4About)
admin.site.register(Images4Rent)
admin.site.register(Images4Opt)
admin.site.register(OrderCall)
admin.site.register(FranchiseOrder)
admin.site.register(FranchiseClient)
admin.site.register(FranchiseGood)
admin.site.register(FranchiseTimer)
admin.site.register(FranchiseWork)
