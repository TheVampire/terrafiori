from django.conf.urls import url
from django.views.generic import TemplateView
from views import *

from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^item/price/type/$', set_price_type, name='price-type-set'),
    url(r'^items/buy/$', BuyView.as_view(), name='item-buy'),
    url(r'^items/category-(?P<slug>[-\w]+)/$', CategoryItemsView.as_view(), name='category-items'),
    url(r'^items/spec-(?P<slug>[-\w]+)/$', SpecOfferItemsView.as_view(), name='spec-items'),
    url(r'^items/(?P<slug>[-\w]+)/$', ItemDetail.as_view(), name='item-detail'),
#    url(r'^items/spec/$', ItemsView.as_view(), name='spec-items'),
    url(r'^origin-item-detail/$', TemplateView.as_view(template_name='origin/item-detail.html'), name='origin-item-detail'),
    url(r'^origin-items/$', TemplateView.as_view(template_name='origin/items.html'), name='origin-items'),
    url(r'^items/$', ItemsView.as_view(), name='items'),
    url(r'^news/$', NewsListView.as_view(), name='news'),
    url(r'^news/(?P<pk>\d+)/$', NewsDetailView.as_view(), name='news-item'),

    url(r'^$', HomeView.as_view(), name='main'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^about/team/$', AboutTeamView.as_view(), name='team'),
    url(r'^about/vacancy/$', AboutVacancyView.as_view(), name='vacancy'),
    url(r'^about/portfolio/$', AboutPortfolioView.as_view(), name='portfolio'),

    url(r'^delivery/way/$', DeliveryWayView.as_view(), name='delivery-way'),
    url(r'^delivery/calc/$', DeliveryCalcView.as_view(), name='delivery-calc'),
    url(r'^delivery/payway/$', PayWayView.as_view(), name='delivery-payway'),
    url(r'^delivery/feedback/$', FeedbackView.as_view(), name='delivery-feedback'),
    url(r'^delivery/support/$', SupportView.as_view(), name='delivery-support'),

    url(r'^search/$', SearchView.as_view(), name='search-view'),
    url(r'^login/ajax/$', LoginAjaxView.as_view(), name='login-ajax-view'),

    url(r'^order/call/$', OrderCallView.as_view(), name='order-call-view'),


    url(r'^contacts/$', ContactsView.as_view(), name='contacts'),
    url(r'^feedback/ajax/$', FeedbackAjaxView.as_view(), name='feedback-ajax'),

    url(r'^login/$', terralogin, name='auth_login'),
    url(r'^logout/$', terralogout, name='auth_logout'),

    url(r'^registration/opt/$', RegistrationOptView.as_view(), name='registration_opt'),
    # url(r'^registration/opt/ajax/$', RegistrationOptAjaxView.as_view(), name='registration_opt_ajax'),
    url(r'^registration/rent/$', RegistrationRentView.as_view(), name='registration_rent'),
    # url(r'^registration/rent/ajax/$', RegistrationRentAjaxView.as_view(), name='registration_rent_ajax'),
    # url(r'^registration/profile/$', ProfileView.as_view(), name='profile'),

    url(r'^private/profile/$', ProfileView.as_view(), name='profile'),
    url(r'^private/order/(?P<pk>\d+)/$', OrderDetailView.as_view(), name='profile_order'),

    url(r'^cart/size/$', get_cart_size, name='cart_size'),
    url(r'^cart/clear/$', csrf_exempt(clear_cart), name='clear_cart'),
    url(r'^cart/add/$', csrf_exempt(add_to_cart), name='add_to_cart'),
    url(r'^cart/item/del/(?P<product_id>\d+)/$', csrf_exempt(remove_from_cart), name='remove_from_cart'),
    url(r'^cart/$', CartView.as_view(), name='cart'),
    url(r'^promo/check/$', csrf_exempt(check_promo), name='check_promo'),
    url(r'^city/check/$', csrf_exempt(get_delivery_price), name='check_city'),

    url(r'^order/add/$', CreateOrderView.as_view(), name='order-add'),

    url(r'^subscribe/ajax/$', SubscribeAjaxView.as_view(), name='subscribe-ajax'),

    url(r'^franchise/$', FranchiseView.as_view(), name='franchise-view'),
    ]