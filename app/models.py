# coding: utf-8
from django.db import models
from cart.models import Cart
from django.core.mail import mail_admins, send_mail
from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User, UserManager
from django.contrib.auth.models import BaseUserManager,AbstractBaseUser, PermissionsMixin
from redactor.fields import RedactorField
from app.templatetags.price_type_tags import cart_summary

# Create your models here.

class Images4Contacts(models.Model):
    image = models.ImageField(upload_to='upload/', verbose_name=u'Изображение')

    def __unicode__(self):
        return u'Изображение {}'.format(self.pk)

    class Meta:
        verbose_name = u'Изображение для страницы Контакты'
        verbose_name_plural = u'Изображения для страницы Контакты'

class Images4About(models.Model):
    image = models.ImageField(upload_to='upload/', verbose_name=u'Изображение')

    def __unicode__(self):
        return u'Изображение {}'.format(self.pk)

    class Meta:
        verbose_name = u'Изображение для страницы О нас'
        verbose_name_plural = u'Изображения для страницы О нас'

class Images4Rent(models.Model):
    image = models.ImageField(upload_to='upload/', verbose_name=u'Изображение')

    def __unicode__(self):
        return u'Изображение {}'.format(self.pk)

    class Meta:
        verbose_name = u'Изображение для страницы Аренда'
        verbose_name_plural = u'Изображения для страницы Аренда'

class Images4Opt(models.Model):
    image = models.ImageField(upload_to='upload/', verbose_name=u'Изображение')

    def __unicode__(self):
        return u'Изображение {}'.format(self.pk)

    class Meta:
        verbose_name = u'Изображение для страницы Опт'
        verbose_name_plural = u'Изображения для страницы Опт'


class DeliveryTextBlock(models.Model):
    slug = models.CharField(max_length=256, verbose_name=u'Slug')
    text = RedactorField(verbose_name=u'Текст', blank=True)

    def __unicode__(self):
        return self.slug

    class Meta:
        verbose_name = u'Текстовый блок для страниц доставки'
        verbose_name_plural = u'Текстовые блоки для страниц доставки'

class AboutTextBlock(models.Model):
    slug = models.CharField(max_length=256, verbose_name=u'Slug')
    text = RedactorField(verbose_name=u'Текст', blank=True)

    def __unicode__(self):
        return self.slug

    class Meta:
        verbose_name = u'Текстовый блок для страниц О нас'
        verbose_name_plural = u'Текстовые блоки для страниц О нас'

class AboutPortfolioItem(models.Model):
    photo = models.ImageField(upload_to=u'uploads/', verbose_name=u'Изображение')
    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    created = models.DateTimeField(verbose_name=u'Дата')
    desc = models.TextField(verbose_name=u'Описание', blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Портфолио: работа'
        verbose_name_plural = u'Портфолио: работы'

class TeamMember(models.Model):
    photo = models.ImageField(upload_to=u'uploads/', verbose_name=u'Фотография')
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    title = models.CharField(max_length=255, verbose_name=u'Должность', blank=True)
    desc = models.TextField(verbose_name=u'Описание', blank=True)
    vk_link = models.CharField(max_length=255, verbose_name=u'Ссылка на VK.com', blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Член команды'
        verbose_name_plural = u'Члены команды'


class OrderCity(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'Название')
    price = models.FloatField(verbose_name=u'Стоимость', default=350)
    days_min = models.IntegerField(verbose_name=u'Дней (от)', default=1)
    days_max = models.IntegerField(verbose_name=u'Дней (до)', default=365)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Город для заказа'
        verbose_name_plural = u'Города для заказа'

class UserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            username=username,
            )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(email,
                                password=password,
                                username=username
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='E-mail',
        max_length=255,
        unique=True,
        db_index=True,)
    username = models.CharField(verbose_name='Ник',  max_length=255, blank=True)
    first_name = models.CharField(verbose_name='Имя',  max_length=255, blank=True)
    last_name = models.CharField(verbose_name='Фамилия',  max_length=255, blank=True)
    date_of_birth = models.DateField(verbose_name='День рождения',  blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    is_rozn = models.BooleanField(verbose_name=u'Клиент по рознице', default=True)
    is_rent = models.BooleanField(verbose_name=u'Клиент по аренде', default=False)
    is_opt = models.BooleanField(verbose_name=u'Клиент по опту', default=False)

    inn = models.CharField(max_length=256, verbose_name=u'ИНН (только для юр. лиц)', blank=True)

    middle_name = models.CharField(verbose_name=u'Отчество', max_length=256, blank=True)

    phone = models.CharField(max_length=100, verbose_name=u'Телефон', blank=True)
    idx = models.CharField(max_length=6, verbose_name=u'Индекс', blank=True, null=True)
    address = models.CharField(max_length=256, verbose_name=u'Адрес', blank=True, null=True)

    city = models.ForeignKey(OrderCity, verbose_name=u'Город', null=True)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name,)

    def get_short_name(self):
        return self.username

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

class MailTemplate(models.Model):
    slug = models.CharField(verbose_name=u'Slug', max_length=100)
    subject = models.CharField(max_length=256, verbose_name=u'Заголовок письма')
    text = models.TextField(verbose_name=u'Тело письма')

    def __unicode__(self):
        return self.slug

    class Meta:
        verbose_name = u'Шаблон письма'
        verbose_name_plural = u'Шаблоны писем'

class PromoCode(models.Model):
    code = models.CharField(max_length=100, verbose_name=u'Код')
    sum = models.FloatField(default=100.0, verbose_name=u'Скидка в процентах')

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = u'Промокод'
        verbose_name_plural = u'Промокоды'

class Order(models.Model):
    user = models.ForeignKey(CustomUser, verbose_name=u'Клиент', null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    cart = models.ForeignKey(Cart)
    first_name = models.CharField(verbose_name=u'Имя', max_length=100)
    last_name = models.CharField(verbose_name=u'Фамилия', max_length=100)
    middle_name = models.CharField(verbose_name=u'Отчество', max_length=100)

    email = models.CharField(max_length=100, verbose_name=u'Email')
    phone = models.CharField(verbose_name=u'Телефон', max_length=100)
    city = models.ForeignKey(OrderCity, verbose_name=u'Город', blank=True, null=True)

    inn = models.CharField(max_length=256, verbose_name=u'ИНН', blank=True)

    idx = models.IntegerField(verbose_name=u'Индекс', blank=True, null=True)
    address = models.CharField(max_length=256, verbose_name=u'Адрес', blank=True)

    comment = models.TextField(verbose_name=u'Комментарий', blank=True)
    promo = models.ForeignKey(PromoCode, verbose_name=u'Промо-код', null=True, blank=True)

    payed = models.BooleanField(default=False, verbose_name=u'Оплачен')

    pay_type = models.CharField(verbose_name=u'Способ оплаты', default=u'Robokassa', max_length=256)

    def total_sales(self):
        if self.promo:
            s = self.summary()
            s = float(s)
            return self.promo.sum/100.0*s
        return 0

    def total_summary(self):
        s = self.summary()
        s = float(s)
        if self.promo:
            s -= self.promo.sum/100.0*s
        if self.city:
            s += self.city.price
        return s

    def get_user_status(self):
        if self.user:
            if self.user.is_rent:
                return u'Аренда'
            elif self.user.is_opt:
                return u'Опт'
        return u'Розница'

    def get_status(self):
        if self.payed: return u'Оплачен'
        return u'Оформлен'

    def save(self, *args, **kwargs):
        is_first = self.pk == None
        user,created = CustomUser.objects.get_or_create(
            email = self.email
        )
        user.first_name = self.first_name
        user.last_name = self.last_name
        user.middle_name = self.middle_name
        user.idx = self.idx
        user.inn = self.inn
        user.phone = self.phone
        user.address = self.address
        user.city = self.city

        if created:
            password = CustomUser.objects.make_random_password()
            user.set_password(password)

            tmpl = MailTemplate.objects.get(slug='user_created')
            subject = Template(tmpl.subject).render(Context({'user': user, 'password': password}))
            text = Template(tmpl.text).render(Context({'user': user, 'password': password}))
            send_mail(subject, text, 'shop@terrafiori.com', [user.email])

        user.save()
        self.user = user
        super(Order, self).save(*args, **kwargs)
        if is_first:
            tmpl = MailTemplate.objects.get(slug='client_order_created')
            subject = Template(tmpl.subject).render(Context({'order': self}))
            text = Template(tmpl.text).render(Context({'order': self}))
            mail_admins(subject, text)

            tmpl = MailTemplate.objects.get(slug='manager_order_created')
            subject = Template(tmpl.subject).render(Context({'order': self}))
            text = Template(tmpl.text).render(Context({'order': self}))
            send_mail(subject, text, 'shop@terrafiori.com', [self.email])

    def summary(self):
        return cart_summary(self.cart, self.user)

    def __unicode__(self):
        return u'Заказ #{} [{}]'.format(self.pk, self.email)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'


class TextBlock(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'Название')
    slug = models.CharField(max_length=100, verbose_name=u'Slug')
    text = models.TextField(blank=True, verbose_name=u'Текст')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Текстовый блок'
        verbose_name_plural = u'Текстовые блоки'

class Banner(models.Model):
    image = models.ImageField(upload_to='uploads/', verbose_name=u'Изображение')
    link = models.CharField(max_length=1024, verbose_name=u'Ссылка на товар')

    def __unicode__(self):
        return u'Banner: {}'.format(self.link)

    class Meta:
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннеры'


class News(models.Model):
    title = models.CharField(max_length=1024, verbose_name=u'Заголовок')
    created = models.DateTimeField(verbose_name=u'Дата создания')
    short_desc = models.TextField(verbose_name=u'Краткое описание', blank=True)
    text = models.TextField(verbose_name=u'Текст статьи')
    num_comments = models.IntegerField(verbose_name=u'Число комментариев', default=0)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

class NewsPhoto(models.Model):
    photo = models.ImageField(upload_to='uploads/', verbose_name=u'Фотография')
    news = models.ForeignKey(News, verbose_name=u'Новость', related_name='images')

    def __unicode__(self):
        return 'Photo #{}'.format(self.pk)

    class Meta:
        verbose_name = u'Фотография для новости'
        verbose_name_plural = u'Фотографии для новостей'


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'Название')
    slug = models.CharField(verbose_name=u'Slug', unique=True, max_length=200)
    image = models.ImageField(upload_to='upload/', verbose_name=u'Обложка категории', blank=True)
    big_image = models.ImageField(upload_to='upload/', verbose_name=u'Фоновая картинка для страницы каталога', blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Рубрика'
        verbose_name_plural = u'Рубрики'

class SpecOffer(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'Название')
    slug = models.CharField(verbose_name=u'Slug', unique=True, max_length=200)
    desc = models.TextField(verbose_name=u'Описание', blank=True)

    def get_items(self):
        return self.items.order_by('-pk')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Спецпредложение'
        verbose_name_plural = u'Спецпредложения'

class ItemColor(models.Model):
    name = models.CharField(max_length=256, verbose_name=u'Название')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Цвет товара'
        verbose_name_plural = u'Цвета товаров'

class ItemByNewQuerySetManager(models.Manager):
    def get_queryset(self):
        return super(ItemByNewQuerySetManager, self).get_queryset().order_by('-pk')

class Item(models.Model):
    name = models.CharField(max_length=200, verbose_name=u'Название')
    slug = models.CharField(max_length=255, verbose_name=u'Slug', unique=True)
    art = models.CharField(max_length=100, verbose_name=u'Артикул')
    desc = models.TextField(verbose_name=u'Описание', blank=True)
    price = models.FloatField(verbose_name=u'Цена', blank=True, default=0.0)
    exists = models.BooleanField(default=True, verbose_name=u'В наличии')
    consist = models.CharField(max_length=200, verbose_name=u'Состав')
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    oldprice = models.FloatField(verbose_name=u'Старая цена', default=0.0)

    price_rent = models.FloatField(verbose_name=u'Цена для аренды', blank=True, default=0.0)
    price_wholesale = models.FloatField(verbose_name=u'Цена для опта', blank=True, default=0.0)

    is_leader = models.BooleanField(default=False, verbose_name=u'Лидер продаж')
    is_new = models.BooleanField(default=False, verbose_name=u'Новинка')
    is_sales = models.BooleanField(default=False, verbose_name=u'Скидка')

    specs = models.ManyToManyField(SpecOffer, verbose_name=u'Специальные предложения', related_name='items', null=True,
                                   blank=True)
    categories = models.ManyToManyField(Category, verbose_name=u'Рубрики', related_name='items', null=True, blank=True)

    addon_items = models.ManyToManyField('Item', verbose_name=u'Дополнительные товары', null=True, blank=True)

    is_service = models.BooleanField(verbose_name=u'Услуга', default=False)

    colors = models.ManyToManyField(ItemColor, verbose_name=u'Выбор цветов', blank=True, null=True)

    objects = ItemByNewQuerySetManager()

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('item-detail', kwargs={
            'slug': self.slug
        })
    class Meta:
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'

class ItemImage(models.Model):
    item = models.ForeignKey(Item, related_name='images')
    image = models.ImageField(upload_to='upload/', verbose_name=u'Изображение')

    color = models.ForeignKey(ItemColor, verbose_name=u'Для цвета', blank=True, null=True)

    def __unicode__(self):
        return u'Image #{}'.format(self.pk)

    class Meta:
        verbose_name = u'Изображение товара'
        verbose_name_plural = u'Изображения товаров'

class OrderCall(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    email = models.EmailField(verbose_name=u'E-mail')
    phone = models.CharField(max_length=255, verbose_name=u'Phone')

    def __unicode__(self):
        return u'[{} - {}]: {}'.format(self.email, self.phone, self.name)

    def save(self, *args, **kwargs):
        super(OrderCall, self).save(*args, **kwargs)
        msg = u'Name: {}\nPhone: {}\nE-mail: {}'.format(self.name, self.phone, self.email)
        mail_admins(u'Заявка на звонок', msg)

    class Meta:
        verbose_name = u'Заявка на звонок'
        verbose_name_plural = u'Заявки на звонок'

class FranchiseOrder(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    phone = models.CharField(max_length=255, verbose_name=u'Телефон')
    email = models.CharField(max_length=255, verbose_name=u'E-mail')
    city = models.CharField(max_length=255, verbose_name=u'Город', blank=True)
    comment = models.TextField(blank=True, verbose_name=u'Комментарий')

    def __unicode__(self):
        return u'{}: {}'.format(self.name, self.email)

    class Meta:
        verbose_name = u'Заявка на франшизу'
        verbose_name_plural = u'Заявки на франшизу'

class FranchiseTimer(models.Model):
    date = models.CharField(max_length=255, verbose_name=u'Дата окончания', help_text='1/01/2015 00:00:01')

    def __unicode__(self):
        return 'Timer: #{}'.format(self.pk)

    class Meta:
        verbose_name = u'Таймер для франишзы'
        verbose_name_plural = u'Таймеры для франшизы'

class FranchiseClient(models.Model):
    image = models.ImageField(upload_to=u'uploads/')
    order = models.IntegerField(default=1, verbose_name=u'Порядок')

    def __unicode__(self):
        return u'Клиент #{}'.format(self.pk)

    class Meta:
        verbose_name = u'Клиент для франишзы'
        verbose_name_plural = u'Клиенты для франшизы'

class FranchiseGood(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название')
    image = models.ImageField(upload_to=u'uploads/', verbose_name=u'Изображение')
    order = models.IntegerField(default=1, verbose_name=u'Порядок')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Товар для франишзы'
        verbose_name_plural = u'Товары для франшизы'

class FranchiseWork(models.Model):
    image = models.ImageField(upload_to=u'uploads/', verbose_name=u'Изображение')
    name = models.CharField(max_length=255, verbose_name=u'Название компании')
    order = models.IntegerField(default=1, verbose_name=u'Порядок')

    def __unicode__(self):
        return u'Работа #{}'.format(self.pk)

    class Meta:
        verbose_name = u'Работа для франишзы'
        verbose_name_plural = u'Работы для франшизы'

