# coding: utf-8
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View, TemplateView, ListView, DetailView, CreateView
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.core.mail import send_mail
from django.http import HttpResponse
from app.models import *
from django.db.models import Q
from django.shortcuts import render_to_response, redirect
import django_settings
from django.core.urlresolvers import reverse
from app.forms import *
import json
from cart import Cart
from robokassa.forms import RobokassaForm
from django.template.loader import render_to_string
from django.core.mail import mail_admins
from robokassa.signals import result_received
from django.template import Context, Template
import mailchimp
from django.conf import settings
from app.templatetags.price_type_tags import visible_by_user, cart_summary

class SubscribeAjaxView(View):

    def post(self, request, *args, **kwargs):
        if request.POST and request.POST.get('email'):
            email = request.POST.get('email')
            list = mailchimp.utils.get_connection().get_list_by_id(settings.MAILCHIMP_LIST_ID)
            list.subscribe(email, {'EMAIL': email}, double_optin=False)
            return HttpResponse('Subscribed')
        return HttpResponse('Error')

# Header Mixin
class HeaderMixin(object):
    def get_context_data(self, **kwargs):
        ctx = super(HeaderMixin, self).get_context_data(**kwargs)
        ctx['phone'] = django_settings.get('phone')
        ctx['vk_link'] = django_settings.get('vk_link')
        ctx['instagramm_link'] = django_settings.get('instagramm_link')
        ctx['mail_link'] = django_settings.get('mail_link')
        ctx['youtube_link'] = django_settings.get('youtube_link')
        ctx['change_price_rent_msg'] = TextBlock.objects.get(slug='change_price_rent_msg')
        ctx['change_price_opt_msg'] = TextBlock.objects.get(slug='change_price_opt_msg')
        ctx['ordercall_form'] = OrderCallForm()
        return ctx

#Feedback Views
class ContactsView(HeaderMixin, TemplateView):
    template_name = 'contacts.html'

    def get_context_data(self, **kwargs):
        ctx = super(ContactsView, self).get_context_data(**kwargs)
        ctx['form'] = FeedbackForm()
        ctx['contact_info'] = TextBlock.objects.get(slug='contact_info')
        ctx['images'] = Images4Contacts.objects.order_by('?')[:2]
        return ctx

# AjaxMixin
class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
                }
            return self.render_to_json_response(data)
        else:
            return response

#Registration Views
class RegOptAjaxMixin(AjaxableResponseMixin):
    def form_invalid(self, form):
        httpr = render_to_response('registration/errors.html', {'form': form})
        httpr.status_code = 400
        return httpr

    def form_valid(self, form):
        user = form.save()
        password = CustomUser.objects.make_random_password()
        user.set_password(password)
        user.is_opt = True
        user.is_rozn = False
        user.save()

        tmpl = MailTemplate.objects.get(slug='user_created')
        subject = Template(tmpl.subject).render(Context({'user': user, 'password': password}))
        text = Template(tmpl.text).render(Context({'user': user, 'password': password}))
        send_mail(subject, text, 'shop@terrafiori.com', [user.email])

        return self.render_to_json_response({
            'data': user.pk
        })

class RegistrationOptAjaxView(RegOptAjaxMixin, CreateView):
    model = CustomUser
    form_class = CustomerUserRegForm
    success_url = '/'

class RegistrationOptView(HeaderMixin, CreateView):
    model = CustomUser
    form_class = CustomerUserRegForm
    template_name = 'registration/base.html'

    def get_context_data(self, **kwargs):
        ctx = super(RegistrationOptView, self).get_context_data(**kwargs)
        ctx['breadcrumbs'] = self.get_breadcrumbs()
        ctx['opt_condition'] = TextBlock.objects.get(slug='opt_condition')
        ctx['reg_opt_msg'] =TextBlock.objects.get(slug='reg_opt_msg')
        ctx['images'] = Images4Opt.objects.order_by('?')[:2]
        return ctx

    def get_breadcrumbs(self):
        bc = [(u'Главная', reverse('main'))]
        bc.append((u'Опт', reverse('registration_opt')))
        return bc

class RegistrationRentView(HeaderMixin, CreateView):
    model = CustomUser
    form_class = CustomerUserRegForm
    template_name = 'registration/base.html'

    def get_context_data(self, **kwargs):
        ctx = super(RegistrationRentView, self).get_context_data(**kwargs)
        ctx['breadcrumbs'] = self.get_breadcrumbs()
        ctx['rent_condition'] = TextBlock.objects.get(slug='rent_condition')
        ctx['reg_rent_msg'] =TextBlock.objects.get(slug='reg_rent_msg')
        ctx['images'] = Images4Rent.objects.order_by('?')[:2]
        return ctx

    def get_breadcrumbs(self):
        bc = [(u'Главная', reverse('main'))]
        bc.append((u'Аренда', reverse('registration_rent')))
        return bc

class RegistrationRentAjaxView(RegistrationOptAjaxView):
    def form_valid(self, form):
        user = form.save()
        password = CustomUser.objects.make_random_password()
        user.set_password(password)
        user.is_rent = True
        user.is_rozn = False
        user.save()

        tmpl = MailTemplate.objects.get(slug='user_created')
        subject = Template(tmpl.subject).render(Context({'user': user, 'password': password}))
        text = Template(tmpl.text).render(Context({'user': user, 'password': password}))
        send_mail(subject, text, 'shop@terrafiori.com', [user.email])

        return self.render_to_json_response({
            'data': user.pk
        })

class FeedbackAjaxView(RegOptAjaxMixin, FormView):
    form_class = FeedbackForm

    def form_valid(self, form):
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        text = form.cleaned_data['text']

        tmpl = MailTemplate.objects.get(slug='fb_created')
        ctx = Context({'name': name, 'email': email, 'text': text})
        subject = Template(tmpl.subject).render(ctx)
        text = Template(tmpl.text).render(ctx)
        mail_admins(subject, text)

        return self.render_to_json_response({
            'data': 'Send'
        })

# Private Views
class ProfileMixin(HeaderMixin):
    def get_context_data(self, **kwargs):
        ctx = super(ProfileMixin, self).get_context_data(**kwargs)
        ctx['orders'] = self.request.user.order_set.order_by('created')
        ctx['breadcrumbs'] = self.get_breadcrumbs()
        return ctx

    def get_breadcrumbs(self):
        bc = [(u'Главная', reverse('main'))]
        return bc

def terralogin(request):
    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(email=email, password=password)
    if user is not None:
        login(request, user)
        return HttpResponse('Logged')
    return HttpResponse(u'Неправильно указан логин или пароль')

def terralogout(request):
    logout(request)
    if request.GET.get('next'):
        return redirect(request.GET.get('next'))
    else:
        return redirect('/')

class ProfileView(ProfileMixin, UpdateView):
    template_name = 'private/profile.html'
    form_class = CustomUserForm
    # model = CustomUser
    success_url = '/private/profile/'

    def get_object(self, queryset=None):
        return self.request.user

    def get_breadcrumbs(self):
        bc = super(ProfileView, self).get_breadcrumbs()
        bc.append((u'Профиль', reverse('profile')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(ProfileView, self).get_context_data(**kwargs)
        ctx['active_profile'] = 'profile'
        return ctx

class OrderDetailView(ProfileMixin, DetailView):
    template_name = 'private/orders.html'
    model = Order

    def get_breadcrumbs(self):
        bc = super(OrderDetailView, self).get_breadcrumbs()
        bc.append((u'Заказ №{}'.format(self.object.pk), reverse('profile_order', kwargs={'pk': self.object.pk})))
        return bc

def set_price_type(request):
    price_type = request.GET.get('type', 'rozn')
    request.session['price_type'] = price_type
    return HttpResponse('OK')

def get_cart_size(request):
    return HttpResponse(u'{}'.format(Cart(request).count()))

def payment_received(sender, **kwargs):
    order = Order.objects.get(id=kwargs['InvId'])
    order.payed = True
    order.cart.delete()
    order.save()

    tmpl = MailTemplate.objects.get(slug='client_order_paid')
    subject = Template(tmpl.subject).render(Context({'order': order}))
    text = Template(tmpl.text).render(Context({'order': order}))
    mail_admins(subject, text)

    tmpl = MailTemplate.objects.get(slug='manager_order_paid')
    subject = Template(tmpl.subject).render(Context({'order': order}))
    text = Template(tmpl.text).render(Context({'order': order}))
    send_mail(subject, text, 'shop@terrafiori.com', [order.email])

result_received.connect(payment_received)

def get_delivery_price(request):
    if request.POST.get('city_pk'):
        cpk = request.POST.get('city_pk')
        oc_qs = OrderCity.objects.filter(pk=cpk)
        if oc_qs.count() > 0:
            oc = oc_qs[0]
            return HttpResponse(json.dumps({
                'time': u'{}-{} дней'.format(oc.days_min, oc.days_max),
                'price': u'{}'.format(oc.price)
            }), content_type='application/json')
    return HttpResponse('-1')

def check_promo(request):
    if request.POST.get('code'):
        code = request.POST.get('code')
        pc_qs = PromoCode.objects.filter(code=code)
        if pc_qs.count():
            promo = pc_qs[0]
            summ = float(Cart(request).summary())
            data = {
                'id': promo.pk,
                'sum': summ*promo.sum/100.0
            }
            return HttpResponse(json.dumps(data), content_type='application/json')
    return HttpResponse("-1")

def clear_cart(request):
    cart = Cart(request)
    cart.clear()
    return HttpResponse("OK")

def remove_from_cart(request, product_id):
    cart = Cart(request)
    cart.cart.item_set.filter(pk=product_id).delete()
    return HttpResponse("{}".format(cart.summary()))

def add_to_cart(request):
    if request.POST.get('pk'):
        item = Item.objects.get(pk=request.POST.get('pk'))
        count = request.POST.get('count', 1)
        cart = Cart(request)
        if 'item_color' in request.POST:
            color = ItemColor.objects.get(pk=request.POST.get('item_color'))
            cart.add(item, item.price, count, options=[color])
        else:
            cart.add(item, item.price, count)
    return HttpResponse("OK")

class CatalogMenuMixin(HeaderMixin):
    def get_context_data(self, **kwargs):
        ctx = super(CatalogMenuMixin, self).get_context_data(**kwargs)
        ctx['specs'] = SpecOffer.objects.all()
        ctx['categories'] = Category.objects.all()
        ctx['footer_categories'] = Category.objects.order_by('?')[:4]
        ctx['breadcrumbs'] = self.get_breadcrumbs()
        ctx['price_filters'] = self.request.session.get('price_filter')
        ctx['sort_field'] = self.request.session.get('sort_field')
        ctx['phone'] = django_settings.get('phone')
        ctx['news_desc'] = TextBlock.objects.get(slug='news-block-desc-main')
        ctx['cart'] = Cart(self.request)
        ctx['push_good_in_cart'] = TextBlock.objects.get(slug='push_good_in_cart')
        ctx['price_type'] = self.request.session.get('price_type', 'rozn')
        ctx['portfolio'] = AboutPortfolioItem.objects.order_by('?')[:2]
        return ctx

    def get_breadcrumbs(self):
        bc = [(u'Главная', reverse('main'))]
        return bc

class AjaxableResponseCreateOrderMixin(AjaxableResponseMixin):

    def form_valid(self, form):
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            cart = Cart(self.request)
            summ = float(cart_summary(cart.cart, self.request.user))
            if self.object.promo:
                summ -= summ*float(self.object.promo.sum)/100.0
            if self.object.city:
                summ += self.object.city.price
            form = RobokassaForm(initial={
                'OutSum': summ,
                'InvId': self.object.pk,
                'Culture': 'ru',
                'Encoding': 'utf-8'
                })
            data = {
                'pk': self.object.pk,
                'form': render_to_string('payform.html', {
                    'form': form
                })
                }
            return self.render_to_json_response(data)
        else:
            return response

class CreateOrderView(AjaxableResponseCreateOrderMixin, CreateView):
    model = Order
    exclude = ('user',)
    success_url = '/cart/'

    def get_form(self, form_class):
        form = super(CreateOrderView, self).get_form(form_class)
        if self.request.user.is_authenticated() and self.request.user.is_rent:
            form.fields.pop('city')
        return form

    def form_valid(self, form):
        r = super(CreateOrderView, self).form_valid(form)
        if self.request.user.is_authenticated():
            self.object.user = self.request.user
            self.object.save()
        return r

class CartView(CatalogMenuMixin, TemplateView):
    template_name = 'cart.html'

    def get_breadcrumbs(self):
        bc = super(CartView, self).get_breadcrumbs()
        bc.append((u'Корзина', reverse('cart')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(CartView, self).get_context_data(**kwargs)
        initial = {
            'city': '1'
        }
        if self.request.user.is_authenticated():
            u = self.request.user
            initial['idx'] = u.idx
            initial['address'] = u.address
            initial['first_name'] = u.first_name
            initial['last_name'] = u.last_name
            initial['middle_name'] = u.middle_name
            initial['phone'] = u.phone
            initial['email'] = u.email
            initial['inn'] = u.inn
        ctx['order_form'] = OrderForm(initial=initial)
        if self.request.user.is_authenticated() and self.request.user.is_rent:
            ctx['order_form'].fields.pop('city')
        if self.request.user.is_authenticated() and self.request.user.is_opt:
            ctx['order_form'].fields['city'].queryset = ctx['order_form'].fields['city'].queryset.exclude(name=u'Санкт-Петербург').exclude(name=u'Самовывоз')
        return ctx


class NewsListView(CatalogMenuMixin, ListView):
    model = News
    context_object_name = 'news'
    template_name = 'news_list.html'
    paginate_by = 20
    queryset = News.objects.order_by('-created')

    def get_breadcrumbs(self):
        bc = super(NewsListView, self).get_breadcrumbs()
        bc.append((u'Новости', reverse('news')))
        return bc

class NewsDetailView(CatalogMenuMixin, DetailView):
    model = News
    context_object_name = 'n'
    template_name = 'news_detail.html'

    def get_context_data(self, **kwargs):
        ctx = super(NewsDetailView, self).get_context_data(**kwargs)
        n = self.get_object()
        if News.objects.filter(created__gt=n.created).exists():
            ctx['prev_news'] = News.objects.filter(created__gt=n.created).order_by('created')[0]
        if News.objects.filter(created__lt=n.created).exists():
            ctx['next_news'] = News.objects.filter(created__lt=n.created).order_by('-created')[0]
        return ctx

    def get_breadcrumbs(self):
        bc = super(NewsDetailView, self).get_breadcrumbs()
        o = self.get_object()
        bc.append((u'Новости', reverse('news')))
        bc.append((o.title, reverse('news-item', kwargs={'pk': o.pk})))
        return bc

class ItemsView(CatalogMenuMixin, ListView):
    model = Item
    template_name = 'items.html'
    context_object_name = 'items'
    http_method_names = ['get', 'post']
    paginate_by = 20

    def get_queryset(self):
        qs = super(ItemsView, self).get_queryset()
        if self.request.user.is_authenticated():
            user = self.request.user
            if user.is_opt:
                qs = qs.filter(~Q(price_wholesale=0.0))
            elif user.is_rent:
                qs = qs.filter(~Q(price_rent=0.0))
            else:
                qs = qs.filter(~Q(price=0.0))
        else:
            qs = qs.filter(~Q(price=0.0))
        pflrt = self.request.session.get('price_filter')
        if pflrt:
            flrt = Q()
            for i in pflrt:
                _min,_max = i.split('-')
                _min,_max = int(_min),int(_max)
                flrt |= Q(price__gte=_min)&Q(price__lte=_max)
            qs = qs.filter(flrt)
        srt = self.request.session.get('sort_field')
        if srt:
            qs = qs.order_by(srt, '-pk')
        else:
            qs = qs.order_by('-pk')
        return qs

    def get(self, request, *args, **kwargs):
        if request.GET.get('price_type'):
            request.session['price_type'] = request.GET.get('price_type')
        return super(ItemsView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        srt = request.session.get('sort_field', ())
        if request.POST.get('sort_field'):
            request.session['sort_field'] = request.POST.get('sort_field')

        pflrt = request.session.get('price_filter', ())
        if request.POST.get('price_filter'):
            if request.POST.get('price_filter') != 'reset':
                pflrt = request.POST.getlist('price_filter')
                request.session['price_filter'] = pflrt
            else:
                request.session['price_filter'] = ()
        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(ItemsView, self).get_context_data(**kwargs)
        return ctx

    def get_breadcrumbs(self):
        bc = super(ItemsView, self).get_breadcrumbs()
        bc.append((u'Каталог', reverse('items')))
        return bc

class SpecOfferItemsView(ItemsView):
    def get_queryset(self):
        self.spec = SpecOffer.objects.get(slug=self.kwargs.get('slug'))
        return visible_by_user(self.spec.items.all(), self.request.user).order_by('-pk')

    def get_breadcrumbs(self):
        bc = super(SpecOfferItemsView, self).get_breadcrumbs()
        bc.append((self.spec.name, reverse('spec-items', kwargs= {'slug':self.spec.slug})))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(SpecOfferItemsView, self).get_context_data(**kwargs)
        ctx['spec_active'] = SpecOffer.objects.get(slug=self.kwargs.get('slug')).slug
        return ctx

class CategoryItemsView(ItemsView):
    def get_queryset(self):
        self.cat = Category.objects.get(slug=self.kwargs.get('slug'))
        return visible_by_user(self.cat.items.all(), self.request.user).order_by('-pk')

    def get_breadcrumbs(self):
        bc = super(CategoryItemsView, self).get_breadcrumbs()
        bc.append((self.cat.name, reverse('category-items', kwargs= {'slug':self.cat.slug})))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(CategoryItemsView, self).get_context_data(**kwargs)
        ctx['category_active'] = Category.objects.get(slug=self.kwargs.get('slug'))
        return ctx

class ItemDetail(CatalogMenuMixin, DetailView):
    model = Item
    template_name = 'item_detail.html'
    context_object_name = 'item'

    def get_context_data(self, **kwargs):
        ctx = super(ItemDetail, self).get_context_data(**kwargs)
        ctx['item_detail'] = True
        return ctx

    def get_breadcrumbs(self):
        bc = super(ItemDetail, self).get_breadcrumbs()
        o = self.get_object()
        cat = o.categories.all()[0]
        bc.append((u'Каталог', reverse('items')))
        bc.append((cat.name, reverse('category-items', kwargs = {'slug':cat.slug})))
        bc.append((o.name, reverse('item-detail', kwargs = {'slug':o.slug})))
        return bc

class HomeView(CatalogMenuMixin, TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        ctx = super(HomeView, self).get_context_data(**kwargs)
        ctx['footer_categories'] = Category.objects.all().order_by('?')[:4]
        ctx['specs'] = SpecOffer.objects.all()
        ctx['banners'] = Banner.objects.all()
        ctx['news'] = News.objects.order_by('-created')[:3]
        if TextBlock.objects.filter(slug='main_page_text_block').exists():
            ctx['main_page_test_block'] = TextBlock.objects.get(slug='main_page_text_block')
        return ctx

class AboutView(CatalogMenuMixin, TemplateView):
    template_name = 'about/page.html'

    def get_breadcrumbs(self):
        bc = super(AboutView, self).get_breadcrumbs()
        bc.append((u'О нас', reverse('about')))
        bc.append((u'История создания', ''))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(AboutView, self).get_context_data(**kwargs)
        ctx['active_about'] = 'about'
        if AboutTextBlock.objects.filter(slug='about').exists():
            ctx['page'] = AboutTextBlock.objects.get(slug='about')
        return ctx

class AboutTeamView(CatalogMenuMixin, TemplateView):
    template_name = 'about/team.html'

    def get_breadcrumbs(self):
        bc = super(AboutTeamView, self).get_breadcrumbs()
        bc.append((u'О нас', reverse('about')))
        bc.append((u'Команда', u''))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(AboutTeamView, self).get_context_data(**kwargs)
        ctx['active_about'] = 'team'
        ctx['team'] = TeamMember.objects.all()
        return ctx

class AboutPortfolioView(CatalogMenuMixin, TemplateView):
    template_name = 'about/portfolio.html'

    def get_breadcrumbs(self):
        bc = super(AboutPortfolioView, self).get_breadcrumbs()
        bc.append((u'О нас', reverse('about')))
        bc.append((u'Портфолио', u''))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(AboutPortfolioView, self).get_context_data(**kwargs)
        ctx['active_about'] = 'portfolio'
        ctx['fr_clients'] = FranchiseClient.objects.order_by('order')
        ctx['works'] = AboutPortfolioItem.objects.order_by('-created')
        return ctx

class AboutVacancyView(CatalogMenuMixin, TemplateView):
    template_name = 'about/page.html'

    def get_breadcrumbs(self):
        bc = super(AboutVacancyView, self).get_breadcrumbs()
        bc.append((u'О нас', reverse('about')))
        bc.append((u'Вакансии', u''))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(AboutVacancyView, self).get_context_data(**kwargs)
        ctx['active_about'] = 'vacancy'
        if AboutTextBlock.objects.filter(slug='vacancy').exists():
            ctx['page'] = AboutTextBlock.objects.get(slug='vacancy')
        return ctx


class FeedbackView(HeaderMixin, View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            name = request.POST.get('name')
            email = request.POST.get('email')
            text = request.POST.get('text')

            msg = u'Имя: {}\n' \
                  u'E-mail: {}\n' \
                  u'Сообщение: {}'.format(name, email, text)
            title = u'[terrafiori.com] Заполнение заявки обратной связи'
#            send_mail(title, msg, 'info@terrafiori.com', ['lifanov.a.v@gmail.com'])
            send_mail(title, msg, 'robot@terrafiori.com', ['info@terrafiori.com'])
        return HttpResponse('OK')


class BuyView(HeaderMixin, View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            name = request.POST.get('name')
            email = request.POST.get('email')
            text = request.POST.get('text')
            if not request.POST.get('item'):
                return HttpResponse('ERROR')
            item = Item.objects.get(slug=request.POST.get('item'))

            msg = u'Имя: {}\n'.format(name) +\
                  u'E-mail: {}\n'.format(email) +\
                  u'Сообщение: {}\n'.format(text) + \
                  u'Товар: {}'.format(request.build_absolute_uri(reverse('item-detail', kwargs={'slug': item.slug})))
            title = u'[terrafiori.com] Покупка товара'
            send_mail(title, msg, 'robot@terrafiori.com', ['info@terrafiori.com'])
        return HttpResponse('OK')


class DeliveryWayView(CatalogMenuMixin, TemplateView):
    template_name = 'delivery/deliveryway.html'

    def get_breadcrumbs(self):
        bc = super(DeliveryWayView, self).get_breadcrumbs()
        bc.append((u'Варианты доставки', reverse('delivery-way')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryWayView, self).get_context_data(**kwargs)
        ctx['active_delivery'] = 'deliveryway'
        if DeliveryTextBlock.objects.filter(slug='deliveryway').exists():
            ctx['desc_block'] = DeliveryTextBlock.objects.filter(slug='deliveryway')[0].text
        return ctx

class DeliveryCalcView(CatalogMenuMixin, TemplateView):
    template_name = 'delivery/deliverycalc.html'

    def get_breadcrumbs(self):
        bc = super(DeliveryCalcView, self).get_breadcrumbs()
        bc.append((u'Калькулятор доставки', reverse('delivery-calc')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryCalcView, self).get_context_data(**kwargs)
        ctx['active_delivery'] = 'calc'
        if DeliveryTextBlock.objects.filter(slug='delivery-calc').exists():
            ctx['desc_block'] = DeliveryTextBlock.objects.filter(slug='delivery-calc')[0].text
        ctx['order_form'] = OrderForm()
        return ctx

class FeedbackView(CatalogMenuMixin, TemplateView):
    template_name = 'delivery/feedback.html'

    def get_breadcrumbs(self):
        bc = super(FeedbackView, self).get_breadcrumbs()
        bc.append((u'Обратная связь', reverse('delivery-feedback')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(FeedbackView, self).get_context_data(**kwargs)
        ctx['active_delivery'] = 'feedback'
        ctx['form'] = FeedbackForm()
        if DeliveryTextBlock.objects.filter(slug='delivery-feedback').exists():
            ctx['desc_block'] = DeliveryTextBlock.objects.filter(slug='delivery-feedback')[0].text
        return ctx

class PayWayView(CatalogMenuMixin, TemplateView):
    template_name = 'delivery/payway.html'

    def get_breadcrumbs(self):
        bc = super(PayWayView, self).get_breadcrumbs()
        bc.append((u'Способ оплаты', reverse('delivery-payway')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(PayWayView, self).get_context_data(**kwargs)
        ctx['active_delivery'] = 'payway'
        if DeliveryTextBlock.objects.filter(slug='delivery-payway').exists():
            ctx['desc_block'] = DeliveryTextBlock.objects.filter(slug='delivery-payway')[0].text
        return ctx

class SupportView(CatalogMenuMixin, TemplateView):
    template_name = 'delivery/support.html'

    def get_breadcrumbs(self):
        bc = super(SupportView, self).get_breadcrumbs()
        bc.append((u'Тех. Поддержка', reverse('delivery-support')))
        return bc

    def get_context_data(self, **kwargs):
        ctx = super(SupportView, self).get_context_data(**kwargs)
        ctx['active_delivery'] = 'support'
        if DeliveryTextBlock.objects.filter(slug='delivery-support').exists():
            ctx['desc_block'] = DeliveryTextBlock.objects.filter(slug='delivery-support')[0].text
        return ctx

# Search view
class SearchView(ItemsView):
    def get_queryset(self):
        qs = super(SearchView, self).get_queryset()
        if self.request.GET.get('q'):
            return qs.filter(name__icontains=self.request.GET.get('q'))
        else:
            return Item.objects.none()

    def get_breadcrumbs(self):
        bc = super(SearchView, self).get_breadcrumbs()
        bc.append((u'Поиск по запросу "{}"'.format(self.request.GET.get('q')), reverse('search-view')))
        return bc

class OrderCallView(View):
    def post(self, request, *args, **kwargs):
        form = OrderCallForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(u'Наш менеджер свяжется с вами в ближайшее время')
        else:
            return HttpResponse(json.dumps({'errors': form.errors}), content_type='application/json')

class LoginAjaxView(View):
    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        pwd = request.POST.get('password')
        if email and pwd:
            u = authenticate(username=email, password=pwd)
            if u is not None:
                login(request, u)
                return HttpResponse('OK')
            else:
                return HttpResponse(json.dumps({'errors': {u'Ошбика': u'Неверное сочетание логина-пароля'}}), content_type='application/json')
        else:
            return HttpResponse(json.dumps({'errors': {u'Ошбика': u'Заполните все поля'}}), content_type='application/json')

class FranchiseView(CatalogMenuMixin, TemplateView):
    template_name = 'franchise.html'

    def post(self, request, *args, **kwargs):
        form = FranchiseForm(request.POST)
        if form.is_valid():
            fo = form.save()
            msg = u'Имя: {}\nТелефон: {}\nE-mail: {}\nГород: {}\nКомментарий: {}'.format(
                fo.name, fo.phone, fo.email, fo.comment, fo.city
            )
            mail_admins(u'Новая заявка на франшизу', msg)
        return HttpResponse('OK')

    def get_context_data(self, **kwargs):
        ctx = super(FranchiseView, self).get_context_data(**kwargs)
        ctx['form'] = FranchiseForm()
        ctx['is_franchise'] = True
        ctx['fr_timer'] = FranchiseTimer.objects.all()[0]
        ctx['fr_clients'] = FranchiseClient.objects.order_by('order')
        ctx['fr_goods'] = FranchiseGood.objects.order_by('order')
        ctx['fr_works'] = FranchiseWork.objects.order_by('order')
        return ctx