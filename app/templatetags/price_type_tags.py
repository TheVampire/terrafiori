from django import template

register = template.Library()

@register.simple_tag
def get_price(item, user):
    if user and user.is_authenticated():
        if user.is_opt:
            return item.price_wholesale
        if user.is_rent:
            return item.price_rent
    return item.price

@register.filter
def is_visible_price_type(item, price_type):
    if price_type == "rozn" and item.price: return True
    if price_type == "opt" and item.price_wholesale: return True
    if price_type == "rent" and item.price_rent: return True
    return False

@register.filter
def visible_by_user(qs, user):
    if user.is_authenticated():
        if user.is_opt:
            return qs.filter(price_wholesale__gt=0.0)
        if user.is_rent:
            return qs.filter(price_rent__gt=0.0)
    return qs

@register.simple_tag
def cart_total_price(item, user):
    return item.quantity*get_price(item.product, user)

@register.simple_tag
def cart_summary(cart, user):
    return sum([item.quantity*get_price(item.product, user) for item in cart.item_set.all()])
