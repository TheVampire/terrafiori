# -*- coding: utf-8 -*-
from django import forms
from app.models import CustomUser, OrderCall, FranchiseOrder
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class FranchiseForm(forms.ModelForm):
    class Meta:
        model = FranchiseOrder

class FeedbackForm(forms.Form):
    name = forms.CharField(max_length=256, label=u'Имя')
    email = forms.CharField(max_length=256, label=u'E-mail')
    text = forms.CharField(widget=forms.Textarea, label=u'Текст')

class CustomerUserRegForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'middle_name', 'email', 'phone']

class OrderCallForm(forms.ModelForm):
    class Meta:
        model = OrderCall

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('email', 'username')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser

    def clean_password(self):
        return self.initial["password"]

from django.forms import ModelForm
from app.models import Order

class CustomUserForm(ModelForm):
    class Meta:
        model = CustomUser
        fields = ('city', 'first_name', 'last_name', 'middle_name', 'phone', 'email', 'idx', 'address')

class OrderForm(ModelForm):
    class Meta:
        model = Order
        exclude=('user',)
    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields['city'].empty_label = None